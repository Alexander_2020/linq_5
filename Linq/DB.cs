﻿using System;
using System.Collections.Generic;

namespace Linq
{
    public class Db
    {
        public Db()
        {
            Users = new List<User>()
            {
                new User(0, "name0", "patronymic0", "surname0",
                    "970111110", "user0", "pass0", new DateTime(2010, 01, 04)),
                new User(1, "name1", "patronymic1", "surname1",
                    "970111111", "user1", "pass1", new DateTime(2012, 05, 18)),
                new User(2, "name2", "patronymic2", "surname2",
                    "970111112", "user2", "pass2", new DateTime(2010, 10, 27)),
                new User(3, "name3", "patronymic3", "surname3",
                    "970111113", "user3", "pass3", new DateTime(2011, 11, 07)),
                
            };

            Accounts = new List<Account>()
            {
                new Account(0, Users[0].RegistrationDate, 1100, 0),
                new Account(1, Users[0].RegistrationDate.AddYears(1), 2000, 0),
                new Account(2, Users[1].RegistrationDate, 550, 1),
                new Account(3, Users[2].RegistrationDate, 1850, 2),
                new Account(4, Users[3].RegistrationDate, 900, 3),
                new Account(5, Users[3].RegistrationDate.AddMonths(5), 2100, 3)
            };
            

            HistoryOperations = new List<HistoryOperation>()
            {
                new HistoryOperation(0, Accounts[0].CreateDate, 500, OperationType.Income, Accounts[0].Id),
                new HistoryOperation(1, Accounts[0].CreateDate, 600, OperationType.Income, Accounts[0].Id),
                new HistoryOperation(2, Accounts[1].CreateDate, 2000, OperationType.Income, Accounts[1].Id),
                new HistoryOperation(3, Accounts[2].CreateDate, 550, OperationType.Income, Accounts[2].Id),
                new HistoryOperation(4, Accounts[3].CreateDate, 1850, OperationType.Income, Accounts[3].Id),
                new HistoryOperation(5, Accounts[4].CreateDate, 900, OperationType.Income, Accounts[4].Id),
                new HistoryOperation(6, Accounts[5].CreateDate, 2100, OperationType.Income, Accounts[5].Id),
                
                new HistoryOperation(7, Accounts[5].CreateDate.AddDays(5), 100, OperationType.Expense, Accounts[5].Id),
                new HistoryOperation(8, Accounts[5].CreateDate.AddDays(6), 100, OperationType.Income, Accounts[5].Id),
            };
        }

        public List<User> Users { get; }
        public List<Account> Accounts { get; }
        public List<HistoryOperation> HistoryOperations { get; }
    }
}