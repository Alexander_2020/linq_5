﻿using System;

namespace Linq
{
    public class User
    {
        public User(int id, string name, string patronymic, string surname, string phone, string login, string password, DateTime registrationDate)
        {
            Id = id;
            Name = name;
            Patronymic = patronymic;
            Surname = surname;
            Phone = phone;
            Login = login;
            Password = password;
            RegistrationDate = registrationDate;
        }

        public int Id { get; }
        public string Name { get; }
        public string Patronymic { get; }
        public string Surname { get; }
        public string Phone { get; }
        public string Login { get; }
        public string Password { get; }
        public DateTime RegistrationDate { get; }
    }
}