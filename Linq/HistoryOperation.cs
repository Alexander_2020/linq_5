﻿using System;

namespace Linq
{
    public partial class HistoryOperation
    {
        public HistoryOperation(int id, DateTime operationDate, decimal sum, OperationType type, int idAccount)
        {
            Id = id;
            OperationDate = operationDate;
            Sum = sum;
            Type = type;
            IdAccount = idAccount;
        }

        public int Id { get; }
        public DateTime OperationDate { get; }
        public decimal Sum { get; set; }
        public OperationType Type { get; set; }
        public int IdAccount { get; }
    }

    public enum OperationType
    {
        Expense,
        Income
    }
}