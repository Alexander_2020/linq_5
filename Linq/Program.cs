﻿using System;

namespace Linq
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            AtmManager atmManager = new AtmManager();

            // 1. Вывод информации о заданном аккаунте по логину и паролю;
            var userInfoString = atmManager.GetUserInfoByLoginPass("user1", "pass1");
            // 2. Вывод данных о всех счетах заданного пользователя;
            var accountsUserString = atmManager.GetAccountsInfoByUserId(5);
            // 3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
            var accountsAndHistoryUserString =  atmManager.GetAccountsAndHistoryInfoByUserId(3);
            // 4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
            var historyString =  atmManager.GetHistoryInfo();
            // 5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);
            var userInfoBySumString =  atmManager.GetUserBySumInfo(1000);
        }
    }
}