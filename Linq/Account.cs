﻿using System;

namespace Linq
{
    public class Account
    {
        public Account(int id, DateTime createDate, decimal sum, int idOwner)
        {
            Id = id;
            CreateDate = createDate;
            Sum = sum;
            IdOwner = idOwner;
        }

        public int Id { get; }
        public DateTime CreateDate { get; }
        public decimal Sum { get; set; }
        public int IdOwner { get; }
    }
}