﻿using System;
using System.Linq;

namespace Linq
{
    public class AtmManager
    {
        private readonly Db _db = new Db();

        public string GetUserInfoByLoginPass(string login, string pass)
        {
            var user = _db.Users.FirstOrDefault(x => x.Login == login && x.Password == pass);
            return user != null ? Newtonsoft.Json.JsonConvert.SerializeObject(user) : "not found";
        }

        public string GetAccountsInfoByUserId(int userId)
        {
            var accounts = _db.Accounts.Where(u => u.IdOwner == userId);
            return Newtonsoft.Json.JsonConvert.SerializeObject(accounts);
        }
        
        public string GetAccountsAndHistoryInfoByUserId(int userId)
        {
            var accounts = (from a in _db.Accounts
                join h in _db.HistoryOperations on a.Id equals h.IdAccount
                into grouped where a.IdOwner == userId 
                select new
                {
                    Account = a,
                    Hisory = grouped.ToList()
                }).ToList();

            return $"Accounts: {Environment.NewLine}{Newtonsoft.Json.JsonConvert.SerializeObject(accounts)}";
        }

        public string GetHistoryInfo()
        {
            var history = _db.HistoryOperations
                .Where(o=>o.Type == OperationType.Income)
                .Join(_db.Accounts,
                h => h.IdAccount,
                a => a.Id,
                (h, a) => new
                {
                    Operation = h,
                    Account = a
                }).ToList();
            
            return Newtonsoft.Json.JsonConvert.SerializeObject(history);
        }
        
        public string GetUserBySumInfo(decimal sum)
        {
            var users = _db.Accounts
                .Where(a => a.Sum > sum)
                .Join(_db.Users,
                    a => a.IdOwner,
                    u => u.Id,
                    (a, u) => u)
                .Distinct().ToList();
            
            return Newtonsoft.Json.JsonConvert.SerializeObject(users);
        }
    }
}